import request from 'supertest'
import { app } from './app'
import { StatusCodes } from 'http-status-codes'

describe('Testing backend-nodejs', () => {

    it('Should make a succesfull call', async () => {
        await request(app)
            .get('/')
            .expect('Content-Type', 'text/html; charset=UTF-8')
            .expect(StatusCodes.OK)
    })
    it('Should get the favicon', async () => {
        await request(app)
            .get('/favicon.ico')
            .expect('Content-Type', 'image/x-icon')
            .expect(StatusCodes.OK)
    })
    it('Should get a 404 when using a non existing path', async () => {
        await request(app)
            .get('/nonexisting')
            .expect('Content-Type', 'text/plain; charset=utf-8')
            .expect('Content-Length', '9')
            .expect(StatusCodes.NOT_FOUND)
    })
    it('Should get a 422 when posting to /scan without a file', async () => {

        await request(app)
            .post('/scan')
            .expect('Content-Type', 'text/plain; charset=utf-8')
            .expect('Content-Length', '20')
            .expect(StatusCodes.UNPROCESSABLE_ENTITY)
    })
})
