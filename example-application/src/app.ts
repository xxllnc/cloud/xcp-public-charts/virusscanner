import clamav from 'clamav.js'
import dotenv from 'dotenv'
import express, { Express, NextFunction, Request, Response } from 'express'; // express is a nodejs web application framework
import { StatusCodes } from 'http-status-codes'; // used for returning http status code
import multer from 'multer'
import path from 'path'; // nodejs path, to read html file from filesystem
import * as stream from 'stream'

// load environment variables
dotenv.config()

// Get the address of Clamav service from the environment variable `CLAMAV_ADDRESS`
const clamavAddress = process.env.CLAMAV_ADDRESS ?? 'localhost'

// Create a new express web app
export const app: Express = express()

// Add a handler for GET requests on the root path /
app.get('/', (_req: Request, res: Response) => {
    // send the index.html back to the requester
    return res.sendFile(path.join(__dirname, '/index.html'))
})

// a browser will request the favicon by default so return it
app.get('/favicon.ico', (_req: Request, res: Response) => {
    // send the favicon.ico to the requester
    return res.sendFile(path.join(__dirname, '/favicon.ico'))
})

// handle the file that is posted to the scan path
const upload = multer({ storage: multer.memoryStorage() })
app.post('/scan', upload.single('file_to_scan'), (req: Request, res: Response, next: NextFunction) => {

    console.log(`Start scanning file. Using clamav ${clamavAddress}:3310`)

    // First check if clam-av is running.
    clamav.ping(3310, clamavAddress, 1000, (err) => {
        const status = err ? `not available[${err}]` : 'alive'
        console.log(`Clamav: ${clamavAddress}:3310 is ${status}`)
    })

    // Check if file is uploaded. If no file is uploaded send unprocessable entry response
    if (!req.file)
        return res.sendStatus(StatusCodes.UNPROCESSABLE_ENTITY)

    // create a new stream object and push file data to it
    const readStream = new stream.Readable();
    readStream.push(req.file.buffer);
    readStream.push(null);

    // create a scanner and scan the file that is in the stream
    const scanner = clamav.createScanner(3310, clamavAddress)
    scanner.scan(readStream, (err, object, malicious) => {
        if (err) {
            console.log(`Error scanning file: ${object.path}: ${err}`)
            next(err)
        } else if (malicious) {
            console.log(`${object.path}: ${malicious} FOUND`)
            next(new Error('Virus Detected'))
        } else {
            console.log(`${object.path}: OK`)
            res.send("OK")
        }
    })
})

// All other requests will give a 404
app.use((_req, res) => res.sendStatus(StatusCodes.NOT_FOUND))
