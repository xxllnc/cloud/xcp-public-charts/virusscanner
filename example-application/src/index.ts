//  imports
import { app } from './app'

// Set TCP port
const port = 8000

// Start webservice
app.listen(port, () => {
    console.log(`✓    server is running on http://localhost:${port} => Press CTRL-C to stop`)
})
