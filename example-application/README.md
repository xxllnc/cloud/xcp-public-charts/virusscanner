# Example application

In this directory there is a small example that show how clam-av could be used.
This example is a small nodejs application that can upload and scan a file.
The result of the scan is shown and the file is deleted afterwards.

This nodejs application is packed into an docker image.
The docker container of this image will run in the same pod as clam-av.

## Development

To run this app on your local machine nodejs and yarn need to be installed:

- <https://nodejs.org/en/download/current>
- <https://classic.yarnpkg.com/lang/en/docs/install/>

All commands below must be run from within the example-application directory

```bash
cd example-application
```

### Start application

```bash
yarn start
```

The application expects that clam-av is running on port 3310. Use the `docker-compose.yaml` to
start a clam-av container.

Execute the following command from the root folder of this project to start-up the clam-av container

```bash
docker compose up -d
```

### building the application

```bash
yarn build
```
