FROM node:iron-alpine3.18 as production

# Copy example application code to install directory in image
COPY ./example-application /install

# Change directory to install
WORKDIR /install

# Install and build code of example application
RUN yarn install
RUN yarn build

# Copy package.json to app directory and install only production packages
WORKDIR /app
RUN cp -r /install/package.json ./package.json
RUN yarn install --production

# Copy compiled code to app/code directory
WORKDIR /app/code
RUN cp -r /install/build/. .
RUN cp -r /install/src/favicon.ico ./favicon.ico
RUN cp -r /install/src/index.html ./index.html

# clean-up install directory
RUN rm -rf /install

CMD [ "node", "index.js" ]
