# Virus scanner

In this repository there is an explanation on how to deploy clamav virus scanner to XCP and use it.

In the directory `example-application` there is a simple nodejs application.
This example-application has one page where a file can be selected and uploaded to be scanned.
The nodejs application uses the javascript package clamav.js to stream the uploaded file
to a clamav docker container. There is a TCP connection used between the nodejs application
and the clamav docker container. In this example clam-av and the example application are
deployed in two separate pods it is als possible to deploy both containers in one pod.
The main benefit is that there is no network communication. If both pods run in a different az
there are also some additional costs for data transfer.

In the root of this project there is a `Dockerfile` that is used to build a docker image of the example-application.
There is also a `docker-compose.yaml` that can be used to start a local clam-av docker container to test
the example-application on a local machine.

In the `templates` directory are the templates to deploy the example application with the clam-av virus scanner.
This is a very minimal set, because the main purpose is to show how to set-up the virus scanner pod with application and clam-av

## Deployment in two pods

In the `templates` directory there are the `deployment-app.yaml` and `deployment-clam-av.yaml` templates.
With the `deployment-app.yaml` template the deployment manifest for the example-application is created.
With the `deployment-clam-av.yaml` template the deployment manifest for the clam-av virus scanner is created.
The `configmap.yaml` holds the configuration for clam-av and the data is mounted in the `deployment-clam-av.yaml` template.

There is a service for the app `serv ice-app.yaml` and a service `service-clam-av.yaml` for clam-av.

For the app there is a `ingress.yaml` so it is exposed to the world

Default all ports are closed on XCP, with `networkpolicies.yaml` the ports are opened for the application and clam-av.

Use the following command to deploy the example-application and the clam-av virus scanner:

```bash
helm upgrade --install test .
```

Optional create a `values.yaml` and set the `clamav.databaseMirror.host` to the correct value

## Deployment in one pod

An other option is to run clam-av and the example-application in one pod. The main bennefit is that there is no
additional network traffic and there are no additional costs.

To combine the two use the following deployment.yaml

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "fullname" (list . .Values.name) }}
  labels:
    {{- include "common.labels" (list . .Values.name) | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  strategy:
    {{- .Values.strategy | toYaml | nindent 4 }}
  selector:
    matchLabels:
      {{- include "selectorLabels" (list . .Values.name) | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "selectorLabels" (list . .Values.name) | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      securityContext:
        runAsNonRoot: true
        runAsUser: 1000
      volumes:
        - name: tmp
          emptyDir: {}
        - name: clamav-dir
          emptyDir: {}
        - name: configuration
          configMap:
            defaultMode: 0444
            name: {{ include "fullname" (list . .Values.name) }}
        - name: initscript
          configMap:
            defaultMode: 0755
            name: {{ include "fullname" (list . .Values.name) }}-init
      containers:
        - name: example-application
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.imagePullPolicy }}
          ports:
          - name: http
            containerPort: {{ .Values.service.port }}
            protocol: TCP
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
        - name: clamav
          securityContext:
            capabilities:
              drop:
                - ALL
            readOnlyRootFilesystem: true
            allowPrivilegeEscalation: false
            runAsNonRoot: true
            runAsUser: 100
            runAsGroup: 101
          image: "{{ .Values.clamav.image.repository }}:{{ .Values.clamav.image.tag }}"
          imagePullPolicy: {{ .Values.clamav.image.pullPolicy }}

          # Temporary - until we have set up proper caching.
          env:
            - name: CLAMAV_NO_FRESHCLAMD
              value: "false"

          volumeMounts:
            - mountPath: /tmp
              name: tmp
            - mountPath: /var/lib/clamav
              name: clamav-dir
            - mountPath: /etc/clamav/clamd.conf
              name: configuration
              subPath: clamd.conf
            - mountPath: /etc/clamav/freshclam.conf
              name: configuration
              subPath: freshclam.conf
            - mountPath: /init
              name: initscript
              subPath: init

          ports:
            - name: clamd
              containerPort: 3310
              protocol: TCP
          livenessProbe:
            tcpSocket:
              port: clamd
            {{- with .Values.clamav.livenessProbe }}
            initialDelaySeconds: {{ .initialDelaySeconds }}
            timeoutSeconds: {{ .timeoutSeconds }}
            periodSeconds: {{ .periodSeconds }}
            failureThreshold: {{ .failureThreshold }}
            {{- end }}
          readinessProbe:
            exec:
              command:
                - /usr/local/bin/clamdcheck.sh
            {{- with .Values.clamav.readinessProbe }}
            initialDelaySeconds: {{ .initialDelaySeconds }}
            timeoutSeconds: {{ .timeoutSeconds }}
            periodSeconds: {{ .periodSeconds }}
            failureThreshold: {{ .failureThreshold }}
            {{- end }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}

      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              topologyKey: topology.kubernetes.io/zone
              labelSelector:
                matchExpressions:
                - key: app.kubernetes.io/name
                  operator: In
                  values:
                    - "{{- .Values.name }}"
                - key: app.kubernetes.io/instance
                  operator: In
                  values:
                    - "{{ .Release.Name }}"
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
```
